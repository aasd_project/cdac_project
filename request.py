import boto3

# Create an S3 client
s3 = boto3.client('s3')

# Create an EC2 client
ec2 = boto3.client('ec2')

# Create an RDS client
rds = boto3.client('rds')

# Create a Lambda client
lambda_client = boto3.client('lambda')

# Create an API Gateway client
api_gateway = boto3.client('apigateway')

# Define a function to list S3 buckets
def list_s3_buckets():
    response = s3.list_buckets()
    print("S3 Buckets:")
    for bucket in response['Buckets']:
        print(f"- {bucket['Name']}")

# Define a function to list EC2 instances
def list_ec2_instances():
    response = ec2.describe_instances()
    print("EC2 Instances:")
    for reservation in response['Reservations']:
        for instance in reservation['Instances']:
            print(f"- {instance['InstanceId']} ({instance['InstanceType']})")

# Define a function to list RDS instances
def list_rds_instances():
    response = rds.describe_db_instances()
    print("RDS Instances:")
    for instance in response['DBInstances']:
        print(f"- {instance['DBInstanceIdentifier']} ({instance['Engine']})")

# Define a function to list Lambda functions
def list_lambda_functions():
    response = lambda_client.list_functions()
    print("Lambda Functions:")
    for function in response['Functions']:
        print(f"- {function['FunctionName']} ({function['Runtime']})")

# Define a function to list APIs in API Gateway
def list_api_gateways():
    response = api_gateway.get_rest_apis()
    print("API Gateway APIs:")
    for api in response['items']:
        print(f"- {api['name']} ({api['id']})")

# Display menu options
def display_menu():
    print("Choose an AWS service:")
    print("1. S3")
    print("2. EC2")
    print("3. RDS")
    print("4. Lambda")
    print("5. API Gateway")
    print("0. Exit")

# Get user input
def get_user_choice():
    choice = input("Enter your choice (0-5): ")
    return choice

# Main program loop
while True:
    display_menu()
    choice = get_user_choice()

    if choice == '1':
        list_s3_buckets()
    elif choice == '2':
        list_ec2_instances()
    elif choice == '3':
        list_rds_instances()
    elif choice == '4':
        list_lambda_functions()
    elif choice == '5':
        list_api_gateways()
    elif choice == '0':
        print("Exiting program...")
        break
    else:
        print("Invalid choice. Please try again.")
