resource "aws_s3_bucket" "developer-bucket" {
  bucket = "developer-bucket-test"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}
