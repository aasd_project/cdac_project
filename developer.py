# function to raise a request
def raise_request():
    print("You have chosen to raise a request.")
    print()

# function to view pending requests
def view_pending_requests():
    print("You have chosen to view pending requests.")
    print()

# function to show credentials
def show_credentials():
    print("You have chosen to show your credentials.")
    print()

# main function
def main():
    print("Welcome, Developer ")
    while True:
        print("1. Raise a request")
        print("2. View pending requests")
        print("3. Show credentials")
        print("4. Logout")

        choice = input("Enter your choice (1-4): ")

        if choice == "1":
            raise_request()
        elif choice == "2":
            view_pending_requests()
        elif choice == "3":
            show_credentials()
        elif choice == "4":
            print("logged out Successfully !")
            break
        else:
            print("Invalid choice. Please try again.")

# Call the main function
main()
