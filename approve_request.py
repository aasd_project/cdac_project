import boto3
# Create an IAM client
iam = boto3.client('iam')

# Define the IAM user name
user_name = 'my_s3_user.10.112.u'

# Create the IAM user
response = iam.create_user(UserName=user_name)
